import firebase from 'firebase-config';
import { galleriesTest } from 'artwork.js';
const firestore = firebase.firestore();
const database = firestore.collection('galleries');


export const seedDataToFirebase = () => {
    const galleries = galleriesTest.map(item => ({
        identifier: item.identifier,
        data: item.data
    }));
    galleries.forEach(async item => {
        await database.add(item);
    })
}

export const getGalleries = async () => {
    const dataGallery = await database.get();
    return dataGallery.docs.map(doc => ({
        id: doc.id,
        ...doc.data()
    }));
}

export const getGalleryById = async (id) => {
    debugger;
    database.get();
    const dataGallery = await database.doc(id).get();
    return dataGallery;
}


export const addGallery = (gallery) => database.add(gallery);