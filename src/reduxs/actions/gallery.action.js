import * as ActionTypes from './action.types';
import * as Services from 'services';

export const setStep = step => dispatch => dispatch({ type: ActionTypes.SET_STEP, step })
export const setBasicForm = basic => dispatch => dispatch({ type: ActionTypes.SET_BASIC_FORM, basic })
export const setLocationForm = location => dispatch => dispatch({ type: ActionTypes.SET_LOCATION_FORM, location })


export const addGallery = gallery => {
  return dispatch => {
    dispatch({ type: ActionTypes.ADD_GALLERY });
    Services.addGallery(gallery)
      .then(() => dispatch({ type: ActionTypes.ADD_GALLERY_SUCCESS }))
      .catch(message =>
        dispatch({ type: ActionTypes.ADD_GALLERY_FAILED, message })
      );
  }
}
