import * as ActionTypes from '../actions/action.types';

const defaultState = {
  step: 1,
  basic: {},
  location: {},
  galleries: [],
  loading: false,
  success: false,
  message: ''
};

export default function galleryReducers(state = defaultState, action) {
  switch (action.type) {
    case ActionTypes.SET_STEP:
      return { ...state, step: action.step }
    case ActionTypes.SET_BASIC_FORM:
      return { ...state, basic: action.basic }
    case ActionTypes.SET_LOCATION_FORM:
      return { ...state, location: action.location }
    case ActionTypes.ADD_GALLERY:
      return { ...state, loading: true, message: '' };
    case ActionTypes.ADD_GALLERY_SUCCESS:
      return { ...state, loading: false, success: true };
    case ActionTypes.ADD_GALLERY_FAILED:
      return { ...state, message: action.message, success: false, loading: false };
    default:
      return state;
  }
}
