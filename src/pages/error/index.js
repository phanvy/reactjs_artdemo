import React, { PureComponent } from 'react';


export default class Page404 extends PureComponent {
    render() {
        return <h3>Page Not Found</h3>
    }
}