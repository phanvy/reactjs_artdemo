import React, { Component } from 'react';
import Sidebar from 'components/sidebar';
import ProductDetail from 'components/product-details';
import ProductListing from 'components/product-listing';

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null
        }
    }

    componentDidMount() {
    }

    onClickProduct = product => { this.setState({ product }) };

    render() {
        const { product } = this.state;
        return <div className="wrapper">
            <Sidebar isAdmin />
            <ProductListing onClickProduct={this.onClickProduct} />
            {
                // product && <ProductDetail product={product} />
            }
        </div>
    }
}