import React, { Component } from 'react';
import './login.scss';
import logo from 'assets/images/logo.png';
import bg_img from 'assets/images/bg_img.png';
import firebase from 'firebase-config';
import { Redirect } from 'react-router-dom';
import validator from 'validator';

export default class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            loading: false,
            redirect: false,
            error: null,
            emailFocused: false,
            passwordFocused: false
        }
    }

    //   handleInputChange = event => {
    //     const { emailFocused, passwordFocused } = this.state;
    //     const name = event.target.name;

    //     this.setState({
    //       [name]: event.target.value,
    //       emailFocused: !emailFocused ? name === 'email' : true,
    //       passwordFocused: !passwordFocused ? name === 'password' : true
    //     });
    //   };

    handleInputChange = event => {
        const { emailFocused, passwordFocused } = this.state;
        const name = event.target.name;

        this.setState({
            [name]: event.target.value,
            emailFocused: !emailFocused ? name === 'email' : true,
            passwordFocused: !passwordFocused ? name === 'password' : true
        });
    }
    onLogin = event => {
        event.preventDefault();
        this.setState({ loading: true });
        const { email, password } = this.state;
        firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(async data => {
                this.setState({ redirect: true, loading: false });
                if (data.user.email === "admin@gmail.com") {
                    localStorage.setItem('userLogin', "admin");
                    this.props.history.push('/dashboard');
                } else {
                    localStorage.setItem('userLogin', "user");
                    this.props.history.push('/');
                }
            })
            .catch(err => {
                console.log(err);
                this.setState({ error: err.message, loading: false });
            });
        event.preventDefault();
    }
    isValidEmail = () => {
        const { email, emailFocused } = this.state;
        return (
            !(!email && emailFocused) && !(!validator.isEmail(email) && emailFocused)
        );
    };

    isValidPassword = () => {
        const { password, passwordFocused } = this.state;
        return !(!password && passwordFocused);
    };

    validate = () => {
        const { email, password } = this.state;
        return password && email && validator.isEmail(email);
    };
    // b64DecodeUnicode(str) {
    //     return decodeURIComponent(Array.prototype.map.call(atob(str), function (c) {
    //         return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    //     }).join(''));
    // }

    // login(e) {
    //     e.preventDefault();
    //     firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((u) => {
    //     }).catch((error) => {
    //         console.log(error);
    //     });
    // }

    // signup = (e) => {
    //     e.preventDefault();
    //     firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then(async (u) => {
    //         /// to do
    //     }).then((u) => { console.log(u) })
    //         .catch((error) => {
    //             console.log(error);
    //         })
    // }

    render() {
        const { email, password, error, loading, redirect } = this.state;
        const { from } = this.props.location.state || { from: { pathname: '/' } };
        if (redirect) {
            return <Redirect to={from} />;
        }
        return <div className="bg">
            <div className="logo px-5 py-5">
                <img src={logo} alt="" />
            </div>
            <div className="px-5">
                <div className="row no-gutters">
                    <div className="col-sm-6">
                        <h2>Art Management System</h2>
                        <h2 className="py-2 text-warning">Championing Southeast Asian Art</h2>
                        <div className="login-form">
                            <form>
                                <div className="form-group">
                                    <label>Email:</label>
                                    <input
                                        name="email"
                                        onChange={this.handleInputChange}
                                        value={email}
                                        placeholder="Enter Email"
                                        className={`form-control ${!this.isValidEmail() &&
                                            'is-invalid'}`}
                                        onBlur={() => this.setState({ emailFocused: true })}
                                    />
                                </div>
                                <div className="form-group">
                                    <label>Password:</label>
                                    <input
                                        name="password"
                                        onChange={this.handleInputChange}
                                        value={password}
                                        type="password"
                                        placeholder="Enter Password"
                                        className={`form-control ${!this.isValidPassword() &&
                                            'is-invalid'}`} />
                                </div>
                                <p className="text-danger">{error}</p>

                                <div className="d-flex justify-content-between">
                                    <input type="button" value={loading ? 'Loading...' : 'Login'}
                                        onClick={this.onLogin} className="btn btn-primary" disabled={loading} />
                                    <p>Forgot password</p>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="bg_img col-sm-6">
                        <img className="pl-5 py-5" src={bg_img} alt="" />
                    </div>
                </div>
            </div>
        </div>
    }
}