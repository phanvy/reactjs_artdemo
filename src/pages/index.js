import HomePage from './home';
import LoginPage from './login';
import * as Arts from './arts'
import Dashboard from './dashboard';
import ProductDetails from './details-page';
import NotFound from './error';

export { Dashboard, NotFound, Arts, HomePage, LoginPage, ProductDetails };