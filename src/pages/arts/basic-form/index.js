import React, { Component } from 'react';
import ArtInput from 'components/input';
import '../artform.scss'
import ArtSelectOption from 'components/select-option';
import { galleriesTest } from '../../../artwork.js';
import FileDragDrop from '../../../components/file-drag-and-drop'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import Actions from 'reduxs/actions';

const categoryData = [
    {
        id: '1',
        name: 'Painting'
    },
    {
        id: '2',
        name: 'Sculpture'
    }
];
const artMediumData = [
    {
        id: '1',
        name: 'Small'
    },
    {
        id: '2',
        name: 'Medium'
    },
    {
        id: '3',
        name: 'High'
    }
];
const coCode = [...new Set(galleriesTest.map(x => x.data.coCode))]
    .map(id => ({
        id,
        name: galleriesTest.find(x => x.data.coCode === id).data.coCode
    }));

const orientationData = [
    {
        id: '1',
        name: 'Portrait'
    },
    {
        id: '2',
        name: 'Landscape'
    },
    {
        id: '3',
        name: 'Square'
    }
]
class BasicForm extends Component {
    constructor() {
        super();
        this.state = {
            arn: '',
            title: '',
            year: '',
            artFrameHeight: '',
            artFrameWidth: '',
            artFrameDepth: '',
            artHeigth: '',
            artWidth: '',
            artDepth: '',
            artworkAwards: '',
            artworkWriteup: '',
            descriptiveNotes: '',
            artist: '',
            artCategory: '',
            artMedium: '',
            orientation: '',
            acquisitionDate: '',
            coCode: '',
            thumbnail: '',

        }
    }

    handleInputChange = event => this.setState({ [event.target.name]: event.target.value });
    handleSelectChange = selectedValue => {
        this.setState({ [selectedValue.target.name]: selectedValue.target.value })
    };

    componentDidMount() {
        this.props.setStep(1);
    }

    saveForm = () => {
        const {
            arn,
            title,
            year,
            artFrameHeight,
            artFrameWidth,
            artFrameDepth,
            artHeigth,
            artWidth,
            artDepth,
            artworkAwards,
            artworkWriteup,
            descriptiveNotes,
            artist,
            artCategory,
            artMedium,
            orientation,
            acquisitionDate,
            coCode,
            thumbnail
        } = this.state;
        const basic = {
            arn,
            title,
            year,
            artFrameHeight,
            artFrameWidth,
            artFrameDepth,
            artHeigth,
            artWidth,
            artDepth,
            artworkAwards,
            artworkWriteup,
            descriptiveNotes,
            artist,
            artCategory,
            artMedium,
            orientation,
            acquisitionDate,
            coCode,
            thumbnail
        };

        this.props.setBasicForm(basic);
        this.props.history.push('/arts/location');
    }
    onDropThumbnail = url => {
        this.setState({ thumbnail: url })
    }
    render() {
        return (
            <div className="form_wrapper">
                <div className="column_form">
                    <ArtInput
                        name={"arn"}
                        value={this.state.arn}
                        onChange={this.handleInputChange}
                        lbText={"ARN"}
                        placeholder='Not assigned yet'
                    />
                    <ArtInput
                        name={"acquisitionDate"}
                        value={this.state.acquisitionDate}
                        onChange={this.handleInputChange}
                        lbText={"Acquistion Date"}
                        type="date"
                        placeholder='Not assigned yet'
                    />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"title"}
                        value={this.state.title}
                        onChange={this.handleInputChange}
                        lbText={"Title"}
                        placeholder="Title"
                    />
                    <ArtSelectOption name={"coCode"} data={coCode} onSelectChange={this.handleSelectChange} />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"artist"}
                        value={this.state.artist}
                        onChange={this.handleInputChange}
                        lbText={"Artist"}
                        placeholder={"Artist"}
                    />
                    <ArtInput
                        name={"year"}
                        value={this.state.year}
                        onChange={this.handleInputChange}
                        lbText={"Year"}
                        placeholder={"Year"}
                    />
                </div>
                <div className="column_form">
                    {/* Dropdown */}
                    <ArtSelectOption name={"artCategory"} data={categoryData} onSelectChange={this.handleSelectChange} />
                    {/* Dropdown */}
                    <ArtSelectOption name={"artMediumData"} data={artMediumData} onSelectChange={this.handleSelectChange} />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"artFrameHeight"}
                        value={this.state.artFrameHeight}
                        onChange={this.handleInputChange}
                        lbText={"W/O Frame Height(cm)"}
                        placeholder={"W/O Frame Height(cm)"}
                    />
                    <ArtInput
                        name={"artHeigth"}
                        value={this.state.artHeigth}
                        onChange={this.handleInputChange}
                        lbText={"W Frame Height(cm)"}
                        placeholder={"W Frame Height(cm)"}
                    />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"artFrameWidth"}
                        value={this.state.artFrameWidth}
                        onChange={this.handleInputChange}
                        lbText={"W/O Frame Width(cm)"}
                        placeholder={"W/O Frame Width(cm)"}
                    />
                    <ArtInput
                        name={"artWidth"}
                        value={this.state.artWidth}
                        onChange={this.handleInputChange}
                        lbText={"W Frame ArtWidth(cm)"}
                        placeholder={"W Frame artWidth(cm)"}
                    />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"artFrameDepth"}
                        value={this.state.artFrameDepth}
                        onChange={this.handleInputChange}
                        lbText={"W/O Frame Depth(cm)"}
                        placeholder={"W/O Frame Depth(cm)"}
                    />
                    <ArtInput
                        name={"artDepth"}
                        value={this.state.artDepth}
                        onChange={this.handleInputChange}
                        lbText={"W Frame Depth(cm)"}
                        placeholder={"W Frame Depth(cm)"}
                    />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"artworkAwards"}
                        value={this.state.artworkAwards}
                        onChange={this.handleInputChange}
                        lbText={"Artwork Awards"}
                        placeholder={"artworkAwards"}
                    />
                    {/* Dropdown */}
                    <ArtSelectOption data={orientationData} onSelectChange={this.handleSelectChange} />
                </div>
                <div className="column_form">
                    <ArtInput
                        name={"artworkWriteup"}
                        value={this.state.artworkWriteup}
                        onChange={this.handleInputChange}
                        lbText={"Writeup"}
                        placeholder={"Artwork Writeup"}
                    />
                    <ArtInput
                        name={"descriptiveNotes"}
                        value={this.state.descriptiveNotes}
                        onChange={this.handleInputChange}
                        lbText={"Internal Notes"}
                        placeholder={"Internal Notes"}
                    />
                </div>
                {/* Thumnail */}
                <div className="thumbnail">
                    <p>Thumbnail</p>
                    <FileDragDrop onDrop={this.onDropThumbnail} />
                </div>
            </div>
        );
    }
}

export default withRouter(connect(state => ({
    gallery: state.gallery,
    step: state.gallery.step
}), dispatch => bindActionCreators(Actions, dispatch), null, {
    forwardRef: true
})(BasicForm));