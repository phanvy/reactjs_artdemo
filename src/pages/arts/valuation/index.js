import React, { Component } from 'react';
import ArtInput from 'components/input';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import Actions from 'reduxs/actions';
import * as Services from 'services';
import { withRouter } from 'react-router-dom';

class ValuationForm extends Component {
    constructor() {
        super();
        this.state = {
            artMinValue: '',
            artMaxValue: '',
            artValueDate: '',
            artValueBy: '',
            artValueNotes: '',
            contact: {
                person: 'Jacquelyn Tan / Mr Chong',
                phone: '6539 4007',
                email: 'Jacquelyn.tan@uobgroup.com',
            }
        }
    }
    handleInputChange = event => this.setState({ [event.target.name]: event.target.value });
    componentDidMount() {
        this.props.setStep(3);
    }
    saveForm = () => {
        const { artMinValue, artMaxValue, artValueDate, artValueBy, artValueNotes, contact } = this.state;
        const valuation = { artMinValue, artMaxValue, artValueDate, artValueBy, artValueNotes, contact };
        const { location, basic } = this.props;

        const gallery = {
            ...basic,
            location,
            // status,
            ...valuation,
        }
        Services.addGallery({ data: gallery })
            .then(
                rs => {
                    console.log(rs);
                    alert('Add new successfully!');
                    this.props.history.push('/dashboard')
                }
            )
            .catch(err => console.log(err));

    }

    render() {
        return <div className="form_wrapper">
            <ArtInput
                name={"artMinValue"}
                value={this.state.artMinValue}
                onChange={this.handleInputChange}
                lbText={"Min Value(SGD)"}
                placeholder='artMinValue'
            />
            <ArtInput
                name={"artMaxValue"}
                value={this.state.artMaxValue}
                onChange={this.handleInputChange}
                lbText={"Max Value(SGD)"}
                placeholder='artMaxValue'
            />
            <ArtInput
                name={"artValueDate"}
                value={this.state.artValueDate}
                onChange={this.handleInputChange}
                lbText={"Value Date"}
                placeholder='artValueDate'
                type="date"
            />
            <ArtInput
                name={"artValueBy"}
                value={this.state.artValueBy}
                onChange={this.handleInputChange}
                lbText={"Value By"}
                placeholder='artValueBy'
            />
            <ArtInput
                name={"artValueNotes"}
                value={this.state.artValueNotes}
                onChange={this.handleInputChange}
                lbText={"Valua Notes"}
                placeholder='artValueNotes'
            />
        </div>
    }
}
export default withRouter(connect(state => ({
    location: state.gallery.location,
    basic: state.gallery.basic
}), dispatch => bindActionCreators(Actions, dispatch), null, {
    forwardRef: true
})(ValuationForm));