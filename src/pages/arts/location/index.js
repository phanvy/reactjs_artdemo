import React, { Component } from 'react';
import '../artform.scss'
import ArtSelectOption from 'components/select-option';
import { galleriesTest } from '../../../artwork.js';
import ArtInput from 'components/input';
import Actions from 'reduxs/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { withRouter } from 'react-router-dom'

const statusData = [...new Set(galleriesTest.map(x => x.data.status))]
    .map(id => ({
        id,
        name: galleriesTest.find(x => x.data.status === id).data.status
    }));

class LocationForm extends Component {
    constructor() {
        super();
        this.state = {
            floor: '',
            countryISOCode: '',
            building: '',
            room: '',
            office: ''
        }
    }

    handleSelectChange = selectedValue => this.setState({ selectedValue: selectedValue });
    handleInputChange = event => this.setState({ [event.target.name]: event.target.value });

    componentDidMount() {
        this.props.setStep(2);
    }

    saveForm = () => {
        const { floor, countryISOCode, room, building, office } = this.state;
        const location = {
            floor,
            countryISOCode,
            room,
            building,
            office
        };

        this.props.setLocationForm(location);
        this.props.history.push('/arts/valuation');
    }

    render() {
        return (<div className="form_wrapper">
            <div className="row_form">
                <ArtSelectOption data={statusData} onSelectChange={this.handleSelectChange} />
                <ArtInput
                    name={"floor"}
                    value={this.state.floor}
                    onChange={this.handleInputChange}
                    lbText={"Floor"}
                    placeholder='Floor'
                />
            </div>
            <div className="row_form">
                <ArtInput
                    name={"countryISOCode"}
                    value={this.state.countryISOCode}
                    onChange={this.handleInputChange}
                    lbText={"countryISOCode"}
                    placeholder='countryISOCode'
                />
                <ArtInput
                    name={"room"}
                    value={this.state.room}
                    onChange={this.handleInputChange}
                    lbText={"Room"}
                    placeholder='room'
                />
            </div>
            <div className="row_form">
                <ArtInput
                    name={"building"}
                    value={this.state.building}
                    onChange={this.handleInputChange}
                    lbText={"Building"}
                    placeholder='building'
                />
                <ArtInput
                    name={"office"}
                    value={this.state.office}
                    onChange={this.handleInputChange}
                    lbText={"Office"}
                    placeholder='office'
                />
            </div>
        </div>

        );
    }
}

export default withRouter(connect(state => ({
    gallery: state.gallery,
    step: state.gallery.step
}), dispatch => bindActionCreators(Actions, dispatch), null, {
    forwardRef: true
})(LocationForm));