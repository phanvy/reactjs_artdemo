import React, { Component } from 'react';
import Header from 'components/client/header';
import './home.scss';
import Product from 'components/products';
import ProductDetail from 'components/product-details';
import Sidebar from 'components/sidebar';

export default class HomePage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: null
        }
    }

    onHoverProduct = product => { this.setState({ product }) };

    render() {
        const { product } = this.state;
        return <div className="wrapper">
            <Header />
            <Sidebar />
            <Product onHover={this.onHoverProduct} />
            {
                product && <ProductDetail product={product} />
            }
        </div>
    }
}