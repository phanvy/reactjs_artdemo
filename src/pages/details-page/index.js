import React, { Component } from 'react';
import * as Services from 'services';

export default class ProductDetails extends Component {
    constructor() {
        super();
        this.state = {
            gallery: [],
            loading: false
        }
    }
    componentDidMount() {
        this.getGallery(this.props.match.params.id);
    }

    getGallery = async (id) => {
        debugger;
        this.setState({ loading: true });
        let data = await Services.getGalleryById(id);
        this.setState({ gallery: data, loading: false });
    }
    render() {
        return <div>
            details page
        </div>
    }
}