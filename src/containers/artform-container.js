import React, { PureComponent } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import AdSidebar from 'components/ad-sidebar';
import { Arts, NotFound } from 'pages';
import { connect } from 'react-redux';
const { LocationForm, BasicForm, ValuationForm, SearchTag, MoreInfoForm } = Arts;

class ArtFormContainer extends PureComponent {
    _onSubmit = () => {
        const { step } = this.props;
        switch (step) {
            case 1:
                this.basicForm.saveForm();
                break;
            case 2:
                this.locationForm.saveForm();
                break;
            case 3:
                this.valuationForm.saveForm();
                break;
            default:
                break;
        }
    }

    render() {
        return (
            <div className="form-container">
                <AdSidebar onSubmit={this._onSubmit} />
                <Switch>
                    <Route exact path="/arts" render={() => <Redirect to="/arts/basic" />} />
                    <Route exact path="/arts/basic" render={() => <BasicForm wrappedComponentRef={ref => this.basicForm = ref} />} />
                    <Route exact path="/arts/location" render={() => <LocationForm wrappedComponentRef={ref => this.locationForm = ref} />} />
                    <Route exact path="/arts/valuation" render={() => <ValuationForm wrappedComponentRef={ref => this.valuationForm = ref} />} />
                    <Route exact path="/arts/search" component={SearchTag} />
                    <Route exact path="/arts/more-info" component={MoreInfoForm} />
                    <Route exact path="*" component={NotFound} />
                </Switch>
            </div>
        );
    }
}

export default connect(state => ({
    step: state.gallery.step
}))(ArtFormContainer);