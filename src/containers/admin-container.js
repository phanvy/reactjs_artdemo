import React, { Component } from 'react';
import Header from 'components/client/header';
import { Dashboard } from 'pages';
import { Route, Switch, Redirect } from 'react-router-dom';
import ArtFormContainer from './artform-container';
import { AdminRouters } from 'routers/routes-constants';
import ProductDetails from 'pages/details-page';

export default class AdminContainer extends Component {
    render() {
        return (
            <>
                <Header isAdmin />
                <Switch>
                    <Route exact path="/admin" render={() => <Redirect to="/dashboard" />} />
                    <Route exact path="/dashboard" component={Dashboard} />
                    <Route path="/dashboard/details/:id" component={ProductDetails} />
                    <Route exact path={AdminRouters} component={ArtFormContainer} />
                </Switch>
            </>
        );
    }
}