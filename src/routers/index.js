import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { NotFound, HomePage, LoginPage } from 'pages';
import { AdminContainer } from 'containers';
import { AdminRouters } from './routes-constants';

export default class Routes extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route
                        exact
                        path="/"
                        component={HomePage}
                    />
                    <Route
                        path={AdminRouters}
                        component={AdminContainer}
                    />
                    <Route
                        exact
                        path="/login"
                        component={LoginPage}
                    />
                    <Route exact path="*" component={NotFound} />
                </Switch>
            </BrowserRouter>
        );
    }
}
