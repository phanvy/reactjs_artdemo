export const AdminRouters = [
    '/admin', '/dashboard', '/arts', '/arts/basic', '/arts/location',
    '/arts/valuation', '/arts/search', '/arts/more-info', '/dashboard/details/:id'
]