import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { store } from 'reduxs'
import Routes from 'routers';
import 'rc-slider/assets/index.css';
import 'assets/styles/_custom.scss';
import './index.scss';

const App = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);

ReactDOM.render(<App />, document.getElementById('root'));
serviceWorker.unregister();
