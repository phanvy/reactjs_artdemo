import firebase from 'firebase';
import Config from 'commons/configs';
firebase.initializeApp(Config.firebase);

export default firebase;