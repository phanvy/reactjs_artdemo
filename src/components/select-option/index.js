import React, { Component } from 'react';
import './index.scss'

class ArtSelectOption extends Component {
    render() {
        const { name, onSelectChange, data } = this.props;
        return <div className="size-option">
            <select className="form-control select"
                name={name}
                onChange={onSelectChange}>
                {data.map((data) =>
                    <option
                        key={data.id}
                        value={data.id}
                    >
                        {data.name}
                    </option>
                )}
            </select>
            <div className="arrow-down"></div>
        </div>
    }
}

export default ArtSelectOption;