import React, { Component } from 'react';
import './sidebar.scss';
import { IoIosSearch } from 'react-icons/io';
import { IoIosArrowDown } from "react-icons/io";
import Slider from 'rc-slider';
const range = {
    0: '0',
    20: '500',
    40: '1,000',
    60: '5,000',
    80: '10,000',
    100: 'Max'
}
export default class Sidebar extends Component {

    onSlideChange = (e) => {
        const value = range[e].replace(',', '');
    }

    render() {
        return <div className="sidebar">
            {
                this.props.isAdmin
                    ? (<h5>Artwork Listing</h5>) : (<h5>Gallery</h5>)
            }
            <br />
            <div className="search">
                <input className="form-control" type="text" placeholder="Search..." />
                <button className="btn_search" > <IoIosSearch size={20} /></button>
            </div>
            <div className="size-option">
                <select className="form-control select">
                    <option value="small">Small</option>
                    <option value="mdeium">Medium</option>
                    <option value="large">Large</option>
                </select>
                <div className="arrow-down"></div>
            </div>
            <div className="mt-4">
                <div className="size-title">
                    <p>Size</p>
                    <IoIosArrowDown className="mr-1" data-toggle="collapse" href="#sizeOption" />
                </div>
                <div className="collapse size-choose show" id="sizeOption">
                    <div className="row">
                        <div className="col-sm-4 label">H(cm):</div>
                        <div className="col-sm-4"><input className="size-input" placeholder="From" /> </div>
                        <div className="col-sm-4"><input className="size-input" placeholder="To" /></div>
                    </div>
                    <div className="row">
                        <div className="col-sm-4 label">W(cm):</div>
                        <div className="col-sm-4"><input className="size-input" placeholder="From" /> </div>
                        <div className="col-sm-4"><input className="size-input" placeholder="To" /></div>
                    </div>
                    <div className="row">
                        <div className="col-sm-4 label">D(cm):</div>
                        <div className="col-sm-4"><input className="size-input" placeholder="From" /> </div>
                        <div className="col-sm-4"><input className="size-input" placeholder="To" /></div>
                    </div>
                </div>
            </div>
            <div className="my-4">
                <div className="size-title">
                    <p>Valuation (SGD)</p>
                    <IoIosArrowDown className="mr-1" data-toggle="collapse" href="#priceOption" />
                </div>
                <div className="collapse px-2 pb-3 show" id="priceOption">
                    <Slider min={0} defaultValue={0} marks={range} step={null} trackStyle={{
                        backgroundColor: '#c8af6a'
                    }}
                        dotStyle={{
                            borderColor: '#c8af6a',
                            backgroundColor: '#c8af6a'
                        }}
                        handleStyle={{
                            borderWidth: 1,
                            borderColor: '#c8af6a',
                            backgroundColor: 'transparent'
                        }}
                        railStyle={{
                            backgroundColor: '#c8af6a'
                        }}
                        onChange={this.onSlideChange} />
                </div>
            </div>

            <div className="mt-4">
                <div className="size-title">
                    <p>Orientation</p>
                    <IoIosArrowDown className="mr-1" data-toggle="collapse" href="#orientationOption" />
                </div>
                <div className="collapse px-2 show" id="orientationOption">
                    <div className="orientation-option">
                        <div className="option">
                            <input type="radio" id="portrait" name="radio-group" defaultChecked />
                            <label htmlFor="portrait">
                                <div className="shape-wrapper">
                                    <div className="shape portrait"></div>
                                </div>
                                <div>Portrait</div>
                            </label>
                        </div>

                        <div className="option">
                            <input type="radio" id="landscape" name="radio-group" />
                            <label htmlFor="landscape">
                                <div className="shape-wrapper">
                                    <div className="shape landscape"></div>
                                </div>
                                <div>Landscape</div>
                            </label>
                        </div>

                        <div className="option">
                            <input type="radio" id="square" name="radio-group" />
                            <label htmlFor="square">
                                <div className="shape-wrapper">
                                    <div className="shape square"></div>
                                </div>
                                <div>Square</div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    }
}