import React, { Component } from 'react';
import './product-details.scss';
import { IoIosArrowDown, IoIosArrowForward } from "react-icons/io";


export default class ProductDetail extends Component {F
    render() {
        const { product = {} } = this.props;
        const data = product.data || {};
        console.log(product);
        return <section className="product_details">
            <div className="details">
                <div className="tonggleDetails">
                    <button><IoIosArrowForward /></button>
                </div>
                <div className="info">
                    <h5>Product Details</h5>
                    <p>{data.arn}</p>
                    <p>{data.title}</p>
                    <p>{data.artist}</p>
                    <p>{data.artMedium}</p>
                    <p>Size (with frame) cm</p>
                    <div className="size_frame">
                        <p>H:{data.artHeight} </p>
                        <p>W:{data.artWidth} </p>
                    </div>
                    <p>Size (without frame) cm</p>
                    <div className="size_frame">
                        <p>H:{data.artFrameHeight} </p>
                        <p>W:{data.artFrameWidth} </p>
                    </div>
                    <img src={data.thumbnail} alt="" />
                    <div className="contact" data-toggle="collapse" data-target="#contact" >
                        <p>Contact</p>
                        <IoIosArrowDown />
                    </div>
                    <div id="contact">
                        <p>{data.contact.person}</p>
                        <p>{data.contact.phone}</p>
                        <p>{data.contact.email}</p>
                        <p>{data.location.office}</p>
                    </div>
                    <div className="contact" data-toggle="collapse" data-target="#location" >
                        <p>Location</p>
                        <IoIosArrowDown />
                    </div>
                    <div id="location">
                        <p>{data.location.countryISOCode}</p>
                        <p>{data.location.building}</p>
                    </div>
                </div>
            </div>
        </section>
    }
}