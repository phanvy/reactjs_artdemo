import React, { Component } from 'react';
import './products.scss';

import * as Services from 'services';

export default class Product extends Component {
    constructor() {
        super();
        this.state = {
            galleries: [],
            loading: false
        }
    }
    componentWillUnmount() {
        this.setState({ galleries: [], loading: false });
    }
    // getProducts = () => {
    //     getFn('products')
    //         .then(data => {
    //             this.setState({
    //                 products: data
    //             })
    //         })
    //         .catch(error => console.log(error));
    // }
    async componentDidMount() {
        this.getGalleries();
    }

    getGalleries = async () => {
        this.setState({ loading: true });
        let galleries = await Services.getGalleries();
        this.setState({ galleries, loading: false });
    }

    render() {
        const { onHover } = this.props;
        return <section className="products">
            {this.state.galleries.map(item => (
                <div className="product" onClick={this.displayProductDetail} key={item.id}
                    onMouseEnter={() => onHover(item)}>
                    <span className="status-text">{item.data.status}</span>
                    <div className="thumbnail" style={{ backgroundImage: `url(${item.data.thumbnail})` }} />
                    <div className="info">
                        <div className="author">
                            <div>{item.data.title}</div>
                            <div>{item.data.artist}</div>
                        </div>
                        <label>
                            <input type="checkbox" />
                        </label>
                    </div>
                </div>
            ))}
        </section >
    }
}