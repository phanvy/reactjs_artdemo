import React, { Component } from 'react';
import './ad-sidebar.scss';
import { Link } from 'react-router-dom';
export default class AdSidebar extends Component {
    render() {
        const { onSubmit } = this.props;
        return <section className="left-sidebar">
            <h4>New Artwork OnBoarding</h4>
            <div className="sidebar-content">
                <ul>
                    <li><Link to={"/arts/basic"}>Basic Details</Link></li>
                    <li><Link to={"/arts/location"}>Location</Link></li>
                    <li><Link to={"/arts/valuation"}>Valuation</Link></li>
                    <li><Link to={"/arts/search"}>Search Tag</Link></li>
                    <li><Link to={"/arts/more-info"}>More Information</Link></li>
                </ul>
                <div className="btn_action">
                    <button className="btn_cancel">
                        Cancel
                    </button>
                    <button onClick={onSubmit} className="btn_submit">
                        Submit
                    </button>
                </div>
            </div>
        </section>
    }
}