import React, { Component } from 'react';
import './header.scss';
import logo from 'assets/images/logo.png';
import Menu from 'components/menu';

export default class Header extends Component {
    render() {
        return <header className="top">
            <div className="logo">
                <img src={logo} alt="" />
            </div>
            <h2>Art Management System</h2>
            <Menu />
        </header>
    }
}