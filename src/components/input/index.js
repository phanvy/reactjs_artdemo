import React, { Component } from 'react';
import './input.scss'
class ArtInput extends Component {

    render() {
        const { value, placeholder, name, lbText, onChange, type } = this.props;
        return <div className="form_group">
            <input className="form_field" type={type} placeholder={placeholder} name={name} value={value} onChange={onChange} />
            <label className="form_label">{lbText}</label>
        </div>
    }
}

export default ArtInput;