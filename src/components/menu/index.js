
import React, { Component } from 'react';
import { FaListUl } from 'react-icons/fa';
import './menu.scss'
import { IoIosImage, IoIosMail, IoIosCheckboxOutline, IoMdGrid, IoIosLogOut, IoIosArrowDown } from "react-icons/io";
import firebase from 'firebase-config';
import { Link, withRouter } from 'react-router-dom';

export default withRouter(class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isShowMenu: false,
        }
        this.showMenu = this.showMenu.bind(this);
        this.closeMenu = this.closeMenu.bind(this);
    }
    closeMenu = (event) => {
        this.setState({ isShowMenu: false }, () => {
            document.removeEventListener('click', this.closeMenu);
        });
    }
    showMenu = (event) => {
        event.preventDefault();
        this.setState({ isShowMenu: true }, () => {
            document.addEventListener('click', this.closeMenu);
        });
    }
    logOut = () => {
        firebase.auth().signOut();
        localStorage.removeItem("userLogin");
        this.props.history.push('/login');
    };
    render() {
        const isAdmin = localStorage.getItem("userLogin");
        return (
            <div className="tonggleMenu" >
                <button onClick={this.showMenu}>
                    <FaListUl size={25} />
                </button>
                {
                    this.state.isShowMenu
                        ? (
                            <div className="menu">
                                {
                                    isAdmin === "user" ? (
                                        <ul>
                                            <li><IoIosImage />Galery</li>
                                            <li onClick={this.logOut}><IoIosLogOut />Logout</li>
                                        </ul>
                                    ) : (
                                            <ul>
                                                <li><IoIosImage /><Link to={"/"}>Galery</Link></li>
                                                <li ><IoIosMail />Request</li>
                                                <li data-toggle="collapse" data-target="#manage" className="active">
                                                    <IoIosCheckboxOutline />Manage <IoIosArrowDown />
                                                    <ul className="sub-menu" id="manage">
                                                        <li>Request</li>
                                                        <li>Task</li>
                                                        <li>Artwork</li>
                                                        <li>Artist</li>
                                                        <li>Location</li>
                                                        <li>Gift</li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <IoMdGrid /> <Link to={"/dashboard"}>Dashboard </Link><IoIosArrowDown />
                                                </li>
                                                <li onClick={this.logOut}><IoIosLogOut />Logout</li>
                                            </ul>
                                        )
                                }

                            </div>
                        )
                        : (
                            null
                        )
                }
            </div >
        );
    }
})