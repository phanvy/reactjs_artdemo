import React, { Component } from "react";
import './listing.scss'
import MUIDataTable from "mui-datatables";
import { IoMdDocument, IoIosAdd, IoMdPricetag, IoIosAlbums } from "react-icons/io";
import { Link, withRouter } from "react-router-dom";
import * as Services from 'services';
const columns = [
    {
        name: "thumbnail",
        label: "Thumbnail",
        options: {
            filter: false,
            customBodyRender: (value) => <img src={value} width={30} height={30} alt="" />
        }
    },
    {
        name: "id",
        label: "ARN",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "title",
        label: "Title",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "artist",
        label: "Artist",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "artMedium",
        label: "ArtMedium",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "artHeight",
        label: "Art Height",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "artWidth",
        label: "Art Width",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "artFrameHeight",
        label: "Art Frame Height",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "artFrameWidth",
        label: "Art Frame Width",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "person",
        label: "Contact Name",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "phone",
        label: "Contact Phone",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "email",
        label: "Contact Email",
        options: {
            filter: true,
            sort: true,
        }
    },
    {
        name: "office",
        label: "Contact Office",
        options: {
            filter: true,
            sort: true,
        }
    },
];

const mapGallery = (galleries) => {
    return galleries.map(gallery => ({
        id: gallery.data.arn,
        title: gallery.data.title,
        thumbnail: gallery.data.thumbnail || 'https://i.picsum.photos/id/598/200/300.jpg',
        artist: gallery.data.artist,
        artMedium: gallery.data.artMedium,
        artHeight: gallery.data.artHeight,
        artWidth: gallery.data.artWidth,
        artFrameHeight: gallery.data.artFrameHeight,
        artFrameWidth: gallery.data.artFrameWidth,
        person: gallery.data.contact.person,
        phone: gallery.data.contact.phone,
        email: gallery.data.contact.email,
        office: gallery.data.location.office,
    }))
}

export default withRouter(class ProductListing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            galleries: [],
            loading: false,
            productID: null
        }
        this.options = {
            filterType: 'checkbox',
            print: false,
            onRowClick: this.onRowClick,
            rowsPerPageOptions: [5, 10, 15],
            responsive: 'scrollMaxHeight',
            fixedHeaderOptions: {
                xAxis: false,
                yAxis: true
            },
        };
    }

    async componentDidMount() {
        this.getGalleries();
        // await Services.seedDataToFirebase();
    }


    componentWillUnmount() {
        this.setState({ galleries: [], loading: false });
    }

    getGalleries = async () => {
        this.setState({ loading: true });
        let galleries = await Services.getGalleries();
        galleries = mapGallery(galleries);
        this.setState({ galleries, loading: false });
    }

    onRowClick = (_, rowMeta) => {
        const product = this.state.galleries[rowMeta.rowIndex];
        const { onClickProduct } = this.props;
        onClickProduct(product);
        this.setState({
            productID: product.id
        }, () => {
            this.props.history.push(`dashboard/details/${this.state.productID}`)
        })
    }

    render() {
        const { galleries, loading } = this.state;
        return <section className="pro_list">
            <div className="admin_menu">
                <span >
                    <Link style={{ color: "#f95858" }} to="/arts"> <IoMdDocument size={25} />New</Link>
                </span>
                <span>
                    <IoIosAdd size={25} />
                Add Activity
                </span>
                <span>
                    <IoMdPricetag size={25} />
               Generate Label
                </span>
                <span style={{ color: "#f95858" }}>
                    <IoIosAlbums size={25} />
                Export
                </span>
            </div>
            <MUIDataTable
                data={galleries}
                columns={columns}
                options={this.options}
                scrollFullHeightFullWidth={true}
            />
            {
                loading && <div>Loading...</div>
            }
        </section>
    }
})