import React, { Component } from 'react';
import Dropzone from 'react-dropzone'
import './index.scss'

class FileDrag extends Component {
    state = {
        files: []
    }

    _onDrop = acceptedFiles => {
        const files = [...this.state.files];

        acceptedFiles.forEach(file => {
            const reader = new FileReader()
            reader.onload = () => {
                const binaryStr = reader.result
                console.log(binaryStr);
                file.base64 = reader.result;
                files.push(file);
                this.props.onDrop(file.base64)
            }
            reader.readAsDataURL(file)
        });

        this.setState({ files }, () => {
            this.props.onDropDone && this.props.onDropDone(this.state.files);
        });

    }

    render() {
        const { files } = this.state;
        return (
            <Dropzone onDrop={this._onDrop}>
                {({ getRootProps, getInputProps }) => (
                    <section className="drag-thumbnail">
                        <div {...getRootProps()}>
                            <input {...getInputProps()} />
                            {
                                files.length <= 0 ? <p>Drag & drop files here, or <u>click</u> to select files</p>
                                    : files.map((file, idx) => (<div key={idx}><img src={file.base64} alt="" width="100%" height="150px" className="img_thumbnail" /></div>))
                            }
                        </div>
                    </section>
                )}
            </Dropzone>
        );
    }
}
export default FileDrag;